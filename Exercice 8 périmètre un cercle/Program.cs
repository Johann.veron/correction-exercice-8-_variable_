﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp13
{
    class Program
    {
        static void Main(string[] args)
        {


            Console.WriteLine(" Saissiser le rayon de votre Cercle");
            double radius = double.Parse(Console.ReadLine());

            // Utilisation de la commande "Double" pour les nombres décimaux
            // Utilisation de la commande "Double.Parse" pour convertir les caractères en nombres

            double perimeter = (2 * Math.PI * radius);  // Math.PI  utilisation de la valeur PI
            Console.WriteLine("Le périmètre de votre cercle est : " + perimeter);

            double surface = (Math.PI * Math.Pow(radius, 2)); // Math.pow utilisation d'une puissance 
            Console.WriteLine("La surface du cercle est : " + surface);
            Console.ReadLine(); // attend la réaction de l'utilisation  (Pause de commande)



        }
    }
}
